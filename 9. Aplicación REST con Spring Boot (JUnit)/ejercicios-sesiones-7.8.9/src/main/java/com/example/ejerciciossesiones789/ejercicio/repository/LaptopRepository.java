package com.example.ejerciciossesiones789.ejercicio.repository;

import com.example.ejerciciossesiones789.ejercicio.entities.Laptop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaptopRepository extends JpaRepository<Laptop, Long> {

}
