package com.example.ejerciciossesiones789.ejercicio.controller;

import com.example.ejerciciossesiones789.ejercicio.entities.Laptop;
import com.example.ejerciciossesiones789.ejercicio.repository.LaptopRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;

@RestController
public class LaptopController {

    private final Logger log = LoggerFactory.getLogger(LaptopController.class);

    private final LaptopRepository laptopRepository;

    public LaptopController(LaptopRepository laptopRepository) {
        this.laptopRepository = laptopRepository;
    }

    /**
     * Buscar todas las laptops
     * htpp://localhost:8080/api/laptops
     * @return
     */
    @GetMapping("/api/laptops")
    public List<Laptop> findAll(){
        // recuperar y devolver las laptop de base de datos
        return laptopRepository.findAll();
    }

    /**
     * Buscar un solo libro en base de datos segun su id
     * http://localhost:8081/api/laptops/1
     * http://localhost:8081/api/laptops/2
     * Request
     * Response
     * @param id
     * @return
     */
    @GetMapping("/api/laptops/{id}")
    @ApiOperation("Buscar un libro por clave primaria id Long")
    public ResponseEntity<Laptop> findOneById(@ApiParam("Clave primaria tipo Long") @PathVariable Long id){

        Optional<Laptop> laptopOkOpt = laptopRepository.findById(id); // 3456546456435
        // opcion 1
        if(laptopOkOpt.isPresent())
            return ResponseEntity.ok(laptopOkOpt.get());
        else
            return ResponseEntity.notFound().build();

        // opcion 2
        // return bookOpt.orElse(null);
        // return bookOpt.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Crear un nuevo laptop en base de datos
     * Método POST, no colisiona con findAll porque son diferentes métodos HTTP: GET vs. POST
     * @param laptop
     * @param headers
     * @return
     */
    @PostMapping("/api/laptops")
    public Laptop create(@RequestBody Laptop laptop){
        // guardar la laptop recibido por parametro en la base de datos
        return laptopRepository.save(laptop);
    }

    /**
     * Actualizar  un libro existente en base de datos
     */
    @PutMapping("/api/laptops")
    public ResponseEntity<Laptop> update(@RequestBody Laptop laptop){
        if(laptop.getId() == null ){ // si no tiene id quiere decir que sí es una creación
            log.warn("Trying to update a non existent book");
            return ResponseEntity.badRequest().build();
        }
        if(!laptopRepository.existsById(laptop.getId())){
            log.warn("Trying to update a non existent book");
            return ResponseEntity.notFound().build();
        }

        // El proceso de actualización
        Laptop result = laptopRepository.save(laptop);
        return ResponseEntity.ok(result); // el libro devuelto tiene una clave primaria
    }

    /**
     * Borrar un libro en base de datos
     */
    @ApiIgnore
    @DeleteMapping("/api/laptops/{id}")
    public ResponseEntity<Laptop> delete(@PathVariable Long id){

        if(!laptopRepository.existsById(id)){
            log.warn("Trying to delete a non existent book");
            return ResponseEntity.notFound().build();
        }

        laptopRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    /**
     * Borrar todos las laptops de la base de datos
     */
    @ApiIgnore // ignorar este método para que no aparezca en la documentación de la api Swagger
    @DeleteMapping("/api/laptops")
    public ResponseEntity<Laptop> deleteAll(){
        log.info("REST Request for delete all books");
        laptopRepository.deleteAll();
        return ResponseEntity.noContent().build();
    }

}