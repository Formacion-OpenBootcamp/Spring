package com.example.ejerciciossesiones789.ejercicio;

import com.example.ejerciciossesiones789.ejercicio.entities.Laptop;
import com.example.ejerciciossesiones789.ejercicio.repository.LaptopRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class main {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(main.class, args);
        LaptopRepository repository = context.getBean(LaptopRepository.class);

        System.out.println("¡ Ejercicio corriendo 7, 8 y 9 !");

        // cargar algunas laptops
        Laptop laptop1 = new Laptop(null, "Lenovo","IdeaPad 15IML05","Intel Core i3 10110U 4GB de RAM 256GB SSD, Intel UHD Graphics 620 1920x1080px Windows 11",560.25);
        Laptop laptop2 = new Laptop(null, "ASUS","Vivobook R565ea-uh31t","Intel Core I3 Intel Uhd Graphics 15,6'' RAM 4gb SSD 128gb Windows 10 Home",399.99);
        repository.save(laptop1);
        repository.save(laptop2);
        System.out.println("Numero de laptops en base de datos : " + repository.findAll().size());

    }

}
