package com.example.ejerciciossesiones456.ejercicio.dos.y.tres.repository;

import com.example.ejerciciossesiones456.ejercicio.dos.y.tres.entities.Laptop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaptopRepository extends JpaRepository<Laptop, Long> {

}
