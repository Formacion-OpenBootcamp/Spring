package com.example.ejerciciossesiones456;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

        System.out.println("¡ Ejercicio corriendo 4, 5 y 6 !");

		SpringApplication.run(Application.class, args);

	}

}
