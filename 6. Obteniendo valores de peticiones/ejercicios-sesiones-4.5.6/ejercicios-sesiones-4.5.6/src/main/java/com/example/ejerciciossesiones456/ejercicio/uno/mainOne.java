package com.example.ejerciciossesiones456.ejercicio.uno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class mainOne {

    public static void main(String[] args) {
        SpringApplication.run(mainOne.class, args);
        System.out.println("Hola ejercicio 1!!");

    }

}
