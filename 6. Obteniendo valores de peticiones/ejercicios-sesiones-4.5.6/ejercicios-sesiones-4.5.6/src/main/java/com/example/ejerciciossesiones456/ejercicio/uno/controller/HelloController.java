package com.example.ejerciciossesiones456.ejercicio.uno.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/ejercicio1/hola")
    public String holaMundo(){
        return "Hasta ejercicio 1";
    }

}