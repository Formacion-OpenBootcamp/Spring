package com.example.ejerciciossesiones456.ejercicio.dos.y.tres.controller;

import com.example.ejerciciossesiones456.ejercicio.dos.y.tres.entities.Laptop;
import com.example.ejerciciossesiones456.ejercicio.dos.y.tres.repository.LaptopRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LaptopController {
    private final LaptopRepository laptopRepository;

    public LaptopController(LaptopRepository laptopRepository) {
        this.laptopRepository = laptopRepository;
    }

    // buscar todas las laptops
    /**
     * htpp://localhost:8080/api/laptops
     * @return
     */
    @GetMapping("/api/laptops")
    public List<Laptop> findALl(){
        // recuperar y devolver las laptop de base de datos
        return laptopRepository.findAll();
    }

    // crear un nuevo laptop en base de datos
    @PostMapping("/api/laptops")
    public Laptop create(@RequestBody Laptop laptop){
        // guardar la laptop recibido por parametro en la base de datos
        return laptopRepository.save(laptop);
    }
}