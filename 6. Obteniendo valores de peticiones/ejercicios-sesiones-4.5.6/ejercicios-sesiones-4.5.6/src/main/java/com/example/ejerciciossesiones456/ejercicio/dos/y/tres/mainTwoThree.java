package com.example.ejerciciossesiones456.ejercicio.dos.y.tres;

import com.example.ejerciciossesiones456.ejercicio.dos.y.tres.entities.Laptop;
import com.example.ejerciciossesiones456.ejercicio.dos.y.tres.repository.LaptopRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class mainTwoThree {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(mainTwoThree.class, args);
        LaptopRepository repository = context.getBean(LaptopRepository.class);

        System.out.println("Ejercicio 2 y 3 corriendo !");

        // cargar algunas laptops
        Laptop laptop1 = new Laptop(null, "Lenovo","IdeaPad 15IML05","Intel Core i3 10110U 4GB de RAM 256GB SSD, Intel UHD Graphics 620 1920x1080px Windows 11",560.25);
        Laptop laptop2 = new Laptop(null, "ASUS","Vivobook R565ea-uh31t","Intel Core I3 Intel Uhd Graphics 15,6'' RAM 4gb SSD 128gb Windows 10 Home",399.99);
        repository.save(laptop1);
        repository.save(laptop2);
        System.out.println("Numero de laptops en base de datos : " + repository.findAll().size());

    }

}
