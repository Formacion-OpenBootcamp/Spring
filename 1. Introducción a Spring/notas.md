# **1. Introducción a Spring**

Spring Framework proporciona un modelo integral de programación y configuración para aplicaciones empresariales modernas basadas en Java, en cualquier tipo de plataforma de implementación.

Un elemento clave de Spring es el soporte de infraestructura a nivel de aplicación: Spring se enfoca en la "plomería" de las aplicaciones empresariales para que los equipos puedan enfocarse en la lógica de negocios a nivel de aplicación, sin ataduras innecesarias a entornos de implementación específicos.

## **Características**

Tecnologías centrales : inyección de dependencia, eventos, recursos, i18n, validación, enlace de datos, conversión de tipos, SpEL, AOP.

Pruebas : objetos simulados, marco TestContext, Spring MVC Test, WebTestClient.

Acceso a datos : transacciones, soporte DAO, JDBC, ORM, Marshalling XML.

Marcos web Spring MVC y Spring WebFlux .

Integración : comunicación remota, JMS, JCA, JMX, correo electrónico, tareas, programación, caché.

Idiomas : Kotlin, Groovy, lenguajes dinámicos.

## **¿Qué son los Beans?**

Un Java Bean, también son conocidos un Bean, es una clase simple en Java que cumple con ciertas normas con los nombres de sus propiedades y métodos. Es un componente (similar a una caja) que nos permite encapsular el contenido, con la finalidad de otorgarle una mejor estructura. Que además, permite la reutilización de código mediante a una estructura sencilla.

Se aconseja que un bean cumpla las siguientes condiciones:

    Constructor sin argumentos: aunque ya existe por defecto (no se ve pero está), se aconseja el declararlo.
    Atributos privados.
    Getters&Setters de todos los atributos.
    El bean puede implementar Serializable.
    Requieren una anotación o crear un fichero .xml donde se detalle que es un Bean.

Mediante estos JavaBeans, desarrollamos nuestro modelo de objetos (o modelo de dominio) para la aplicación. Nuestros POJOS realmente son Beans siempre y que añadamos una anotación que indique que dicho POJO es un Bean.

## **Repositorio**

En el siguiente [link](https://github.com/Open-Bootcamp/spring).
