# **Spring**

## **1. Introducción a Spring**

- ### Vídeo del tema 1: Introducción a Spring
- ### Repositorio del curso

## **2. Spring Beans**

- ### Vídeo del tema 2: Spring Beans

## **3. Spring Data**

- ### Vídeo del tema 3: Spring Data - Parte I
- ### Vídeo del tema 3: Spring Data - Parte II
- ### Sesión de dudas
- ### Ejercicios sesiones 1, 2 y 3
- ### Entrega ejercicio 1, 2 y 3

## **4. Spring Boot**

- ### Video del tema 4: Spring Boot

## **5. Aplicaciones REST con Spring Boot**

- ### Video del tema 5: Aplicaciones REST con Spring Boot

## **6. Obteniendo valores de peticiones**

- ### Video del tema 6: Obteniendo valores de peticiones
- ### Sesión de dudas
- ### Ejercicios sesiones 4, 5 y 6
- ### Entrega ejercicios 4, 5 y 6

## **7. Devolviendo respuestas**

- ### Video del tema 7: Devolviendo respuestas

## **8. Documentando con Swagger**

- ### Video del tema 8: Documentando con Swagger

## **9. Aplicación REST con Spring Boot**

- ### Video del tema 9: Aplicación REST con Spring Boot
- ### Ejercicios sesiones 7, 8 y 9
- ### Entrega ejercicios 7, 8 y 9

## **10. Builds en Aplicación REST con Spring Boot**

- ### Video del tema 10: Builds en Aplicación REST con Spring Boot

## **11. Desplegando una Aplicación REST con Spring Boot**

- ### Video del tema 11: Desplegando una Aplicación REST con Spring Boot

## **12. Añadiendo seguridad**

- ### Video del tema 12: Añadiendo seguridad
- ### Ejercicios sesiones 10, 11 y 12
- ### Entrega ejercicios 10, 11 y 12

## **13. Sistemas de cifrado**

- ### Video del tema 13: Sistemas de cifrado

## **14. Autenticación de usuarios con JWT**

- ### Video del tema 14: Autenticación de usuarios con JWT

## **15. Aplicando JWT como método de autenticación**

- ### Video del tema 15: Aplicando JWT como método de autenticación

## **16. Autenticación de usuarios con OAuth 2**

- ### Vídeo del tema 16: Autenticación de usuarios con OAuth 2

## **17. Aplicando OAuth 2 como método de autenticación**

- ### Vídeo del tema 17: Aplicando OAuth 2 como método de autenticación

## **18. Autorización de usuarios**

- ### Vídeo del tema 18: Autorización de usuarios

## **19. Manejo de excepciones**

- ### Vídeo del tema 19: Manejo de excepciones
- ### Sesión de dudas

## **20. Protección ante ataques con programación segura**

- ### Vídeo del tema 20: Protección ante ataques con programación segura

## **21. Patrones de diseño existentes en Spring Security**

- ### Vídeo del tema 21: Patrones de diseño existentes en Spring Security

## **22. Patrón Plantilla**

- ### Vídeo del tema 22: Patrón Plantilla

## **23. Patrón Cadena de Responsabilidad**

- ### Vídeo del tema 23: Patrón de Responsibilidad

## **24. Patrón de Estrategia**

- ### Vídeo del tema 24: Patrón de Estrategia

## **25. Patrón de Proxy**

- ### Vídeo del tema 25: Patrón de Proxy

## **26. Patrón de Constructor**

- ### Vídeo del tema 26: Patrón de Contructor

## **27. Patrón Observador**

- ### Vídeo del tema 27: Patrón de Observador

## **28. Patrón Decorador**

- ### Vídeo del tema 28: Patrón Decorador

## **29. Despliegue de la aplicación**

- ### Vídeo del tema 29: Despliegue de la aplicación

## **30. Mantenimiento en aplicaciones Spring**

- ### Vídeo del tema 20: Mantenimiento en aplicaciones Spring

## **31. Tareas en el manejo de excepciones en Spring**

- ### Vídeo del tema 31: Tareas en el manejo de excepciones en Spring
