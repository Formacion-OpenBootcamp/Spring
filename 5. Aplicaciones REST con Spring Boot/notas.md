# **5. Aplicaciones REST con Spring Boot**

## **Entidad Book**

1. Book
2. BookRepository
3. BookController
   1. Buscar todos los libros
   2. Buscar un solo libro
   3. Crear un nuevo libro
   4. Actualizar un libro existente
   5. Borrar un libro
   6. Borrar todos los libros
