# **11. Desplegando una Aplicación REST con Spring Boot (Deploy in Heroku)**

## **Pasos para deplejar nuetra app en Heroku**

Crear archivo `system.properties` en el proyecto con el contenido:

```
java.runtime.version=17
```

1. Crear un cuenta en heroku.com y github.com
2. Tener configurado git en el ordenador
   - _git config --global user.name "Alan Sastre"_
   - _git config --global user.email "alan@example.com"_
3. Subir el proyecto a GitHub desde Intellij IDEA desde la opcion VCS > Share project on GitHub
4. Desde Heroku, crear app y elegir metodo GitHub y buscar el repositorio subido
5. Habilitar deploy automatico y ejecutar el Deploy
