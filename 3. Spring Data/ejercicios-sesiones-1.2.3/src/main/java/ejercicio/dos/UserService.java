package ejercicio.dos;

import org.springframework.stereotype.Component;

@Component
public class UserService {

    NotificationService notificationService;

    public UserService(NotificationService notificationService) {
        System.out.println("Ejecutando constructor NotificationService");
        this.notificationService = notificationService;
    }

}
