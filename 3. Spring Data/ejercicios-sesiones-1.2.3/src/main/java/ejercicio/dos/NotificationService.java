package ejercicio.dos;

import org.springframework.stereotype.Component;

@Component
public class NotificationService {

    public void saludar(){
        System.out.println("Hola desde NotificationService");
    }

}
