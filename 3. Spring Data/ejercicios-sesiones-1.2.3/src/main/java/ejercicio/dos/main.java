package ejercicio.dos;

import ejercicio.uno.Saludo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class main {

    public static void main(String[] args) {
        
        System.out.println("¡ Ejercicio corriendo 2 !");

        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        UserService userService = (UserService) context.getBean("userService");

        userService.notificationService.saludar();
    }

}
