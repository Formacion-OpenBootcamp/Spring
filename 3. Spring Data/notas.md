# **3. Spring Data**

La misión de Spring Data es proporcionar un modelo de programación familiar y coherente basado en Spring para el acceso a los datos y, al mismo tiempo, conservar las características especiales del almacén de datos subyacente.

Facilita el uso de tecnologías de acceso a datos, bases de datos relacionales y no relacionales, marcos de reducción de mapas y servicios de datos basados ​​en la nube. Este es un proyecto general que contiene muchos subproyectos que son específicos de una base de datos determinada. Los proyectos se desarrollan trabajando en conjunto con muchas de las empresas y desarrolladores que están detrás de estas emocionantes tecnologías.

### **Características**

- Potente repositorio y abstracciones personalizadas de mapeo de objetos
- Derivación de consultas dinámicas a partir de nombres de métodos de repositorio
- Clases base de dominio de implementación que proporcionan propiedades básicas
- Soporte para auditoría transparente (creado, último cambio)
- Posibilidad de integrar código de repositorio personalizado
- Fácil integración de Spring a través de JavaConfig y espacios de nombres XML personalizados
- Integración avanzada con controladores Spring MVC
- Compatibilidad experimental para la persistencia entre tiendas

## **Ejercicios sesiones 1, 2 y 3**

### **Ejercicio 1**

Crear proyecto maven con la dependencia spring-context y crear una clase Saludo con un método imprimirSaludo que imprima un texto por consola.

Crear el archivo beans.xml con un bean para la clase Saludo.

Obtener el bean desde el método main y ejecutar el método imprimirSaludo.

### **Ejercicio 2**

Crear la clase NotificationService, con un método que imprima un saludo.

Crear una clase UserService que tenga un atributo de clase NotificationService.

Utilizar la anotación @Component en cada clase.

Habilitar el escaneo de clases en el archivo beans.xml

Desde el método main, probar a obtener el bean UserService y ejecutar el método imprimirSaludo que tiene asociado el atributo de tipo NotificationService. De forma similar a la realizada en clase.

### **Ejercicio 3**

Crear un proyecto Spring Boot con las dependencias:

Spring Data JPA

H2

En caso de querer que la base de datos se guarde en disco habrá que añadir las siguientes propiedades en el archivo application.properties:

```properties
spring.jpa.show-sql=true
spring.datasource.url=jdbc:h2:file:C:/data/sample
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.driverClassName=org.h2.Driver
#spring.jpa.hibernate.ddl-auto=create
spring.jpa.hibernate.ddl-auto=update
spring.sql.init.mode=always
spring.jpa.defer-datasource-initialization=true
```