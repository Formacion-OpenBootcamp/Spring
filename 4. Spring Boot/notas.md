# **4. Spring Boot**

Spring Boot facilita la creación de aplicaciones basadas en Spring independientes y de grado de producción que puede "simplemente ejecutar".

Tomamos una opinión obstinada de la plataforma Spring y las bibliotecas de terceros para que pueda comenzar con el mínimo esfuerzo. La mayoría de las aplicaciones de Spring Boot necesitan una configuración mínima de Spring.

Si está buscando información sobre una versión específica o instrucciones sobre cómo actualizar desde una versión anterior, consulte la sección de notas de la versión del proyecto en nuestro wiki.

### **Características**

- Cree aplicaciones Spring independientes
- Incruste Tomcat, Jetty o Undertow directamente (no es necesario implementar archivos WAR)
- Proporcione dependencias "iniciales" obstinadas para simplificar su configuración de compilación
- Configure automáticamente Spring y bibliotecas de terceros siempre que sea posible
- Proporcione funciones listas para producción, como métricas, controles de estado y configuración externalizada
- Absolutamente sin generación de código y sin requisitos para la configuración XML

## **Proyecto : ob-rest-datajpa**

Proyecto Spring Boot con las dependencias / starters :

- H2
- Spring Data JPA
- Spring Web
- Spring Boot Dev Tools

Aplicacion API REST con acceso a base de datos H2 para persistir la informacion.

El acceso se puede realizar desde Postman o Navegador.

- Entidad Book
    1. Book
    2. BookRepository
    3. BookController 