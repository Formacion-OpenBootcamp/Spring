# **8. Documentando con Swagger**

## **Edicion sobre el pom.xml**

Agregamos la dependecia que necesitamos para trabajar con Swagger.

```xml
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-boot-starter -->
<dependency>
   <groupId>io.springfox</groupId>
   <artifactId>springfox-boot-starter</artifactId>
   <version>3.0.0</version>
</dependency>
```

## **Configuracion para Swagger**

```java
package com.example.obrestdatajpa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

/**
 * Configuración Swagger para la generación de documentación de la API REST
 *
 * HTML: http://localhost:8081/swagger-ui/
 * JSON: http://localhost:8081/v2/api-docs
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api(){

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiDetails())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiDetails(){
        return new ApiInfo("Spring Boot Book API REST",
                "Library Api rest docs",
                "1.0",
                "http://www.google.com",
                new Contact("Alan", "http://www.google.com", "alan@example.com"),
                "MIT",
                "http://www.google.com",
                Collections.emptyList());
    }
```

## **Etiquetas sobre las clases**

Para documentar aun mas peticiones http, se puede agregar informacion relevante.

### **BookController.java**

Ignorar los metodos con @ApiIgnore

```java
@ApiIgnore // ignorar este método para que no aparezca en la documentación de la api Swaggerger
@DeleteMapping("/api/books/{id}")
public ResponseEntity<Book> delete(@PathVariable Long id){

   if(!bookRepository.existsById(id)){
      log.warn("Trying to delete a non existent book");
      return ResponseEntity.notFound().build();
   }

   bookRepository.deleteById(id);

   return ResponseEntity.noContent().build();
}
```

Describir un parametro con @ApiParam

```java
@GetMapping("/api/books/{id}")
@ApiOperation("Buscar un libro por clave primaria id Long")
public ResponseEntity<Book> findOneById(@ApiParam("Clave primaria tipo Long") @PathVariable Long id){
   Optional<Book> bookOpt = bookRepository.findById(id); // 3456546456435
   if(bookOpt.isPresent())
      return ResponseEntity.ok(bookOpt.get());
   else
      return ResponseEntity.notFound().build();
}
```

### **Book.java**

Describir un atributo con @ApiModelProperty

```java
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@ApiModelProperty("Clave ficticia autoincremental tipo Long")
private Long id;
private String title;
private String author;
private Integer pages;
@ApiModelProperty("Precio en rupias, con dos decimales utiliando . como separador")
private Double price;
private LocalDate releaseDate;
private Boolean online;
```
