package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        // CONCEPTO 1 : como obtener beans de Spring

        // Opcion 1 : Crear un objeto de forma normal
        //Calculadora cal = new Calculadora();

        // Opcion 2 : Recibir un objeto de Spring
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        Calculadora calculadora = (Calculadora) context.getBean("calculadora");
        String texto = calculadora.holaMundo();
        System.out.println(texto);

        // Se puede volver a obtener
        // OJO : se recupera el mismo objeto
        /*
        Calculadora calculadora2 = (Calculadora) context.getBean("calculadora");
        String texto2 = calculadora2.holaMundo();
        System.out.println(texto2);
        */

        // CONCEPTO 2 : cargar un bean dentro de otro bean

        GestorFacturas gestor = (GestorFacturas) context.getBean( "gestorFacturas");
        System.out.println(gestor.calculadora.holaMundo());
        System.out.println(gestor.nombre);

        // CONCEPTO 3 : scope o alcance
        // los beans por defecto con singleton, se crea el objeto y se reutiliza para tada la aplicacion
        // podemos cambiarlo a scope="prototype" si queremos que se cree un nuevo objeto cada vez
        // verificarlo viendo como se ejecuta varias veces un constructor

    }
}
