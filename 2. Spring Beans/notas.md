# **2. Spring Beans**

- Spring es el framework que se va a encargar de crear los objetos a traves de los beans.

## **project : ob-spring-beans**

```java
<bean id="calculadora" class="com.example.Calculadora" scope="prototype">
    <!-- collaborators and configuration for this bean go here -->
</bean>
```

Por defecto los beans son singleton, pero se pueden cambiar con **scope="prototype"**.

## **project : ob-spring-scan**

Los beans se pueden crear desde beans.xml o se puede simplificar a traves de anotaciones.

Ejemplo

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <context:component-scan base-package="com.example"/>

</beans>
```

```java
package com.example;

import org.springframework.stereotype.Component;

@Component
public class Calculadora {

    public Calculadora(){
        System.out.println("Ejecutando constructor Calculadora");
    }

    public String holaMundo (){
        return "Hola mundo!";
    }

}
```
