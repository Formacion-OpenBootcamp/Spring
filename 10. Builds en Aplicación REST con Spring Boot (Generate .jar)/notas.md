# **10. Builds en Aplicación REST con Spring Boot (Generate .jar)**

### **Como generar un ejecutable de nuestra app**

Ir a maven (lateral) > spring-deploy > Lifecycle > package (doble click)

- Se crea el .jar dentro de nuestra carpeta target.

### **Como limpiar nuestro target**

Ir a maven (lateral) > spring-deploy > Lifecycle > clean (doble click)
