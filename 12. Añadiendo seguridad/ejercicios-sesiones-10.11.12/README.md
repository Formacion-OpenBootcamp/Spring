## **Conflicto en pom.xml**

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.5.5</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

- version _<version>2.5.5</version>_

  - _import javax.persistence._ in entities -> OK
  - _WebSecurityConfig_ in controller -> NOT FOUND

- version _<version>3.0.2</version>_
  - _path of app example "/hola"_ -> NOT FOUND
  - _WebSecurityConfig_ in controller -> OK
